(function() {
    'use strict';

    angular
        .module('testApp')
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$interval', 'dataService'];

    function MainController($scope, $interval, dataService) {
        /**
         * The list of rows retrieved.
         * @type {Array}
         */
        $scope.rows = [];

        /**
         * The selected row. 'null' means there is no selection.
         * @type {Object}
         */
        $scope.selectedRow = null;

        $scope.onRefresh = onRefresh;
        $scope.selectRow = selectRow;
        $scope.clearFields = clearFields;

        /**
         * Keep the active interval to refresh the table.
         * @type {Promise}
         */
        var refreshInterval = null;

        initialize();

        /**
         * Initialize the page.
         */
        function initialize() {
            onRefresh();
        }

        /**
         * Refresh the table.
         */
        function refreshTable() {
            // Reset the table.
            $scope.rows = [];

            dataService
                .getRowIds()
                .then(onGetRowIdsSuccess);

            function onGetRowIdsSuccess(res) {
                var found = false;

                _.each(res.rows, function(rowId) {
                    if ($scope.selectedRow && rowId === $scope.selectedRow.id) {
                        found = true;
                    }

                    dataService
                        .getRowDetails(rowId)
                        .then(onGetRowDetailsSucces);
                });

                if (!found) {
                    $scope.selectedRow = null;
                }
            }

            function onGetRowDetailsSucces(res) {
                $scope.rows.push(res.row);
            }
        }

        /**
         * Refresh the table when clicking on 'Refresh' button.
         */
        function onRefresh() {
            if (refreshInterval) {
                $interval.cancel(refreshInterval);
            }

            refreshTable();

            // Refresh the table every 10 seconds.
            refreshInterval = $interval(refreshTable, 10 * 1000);
        }

        /**
         * Select a row when clicking on table rows.
         * @param {Object} row
         */
        function selectRow(row) {
            $scope.selectedRow = _.clone(row);
        }

        /**
         * Clear the edit fields in widget editor.
         * Called when clicking on 'Clear' button.
         */
        function clearFields() {
            $scope.selectedRow.name = '';
            $scope.selectedRow.value = '';
        }
    }
})();
