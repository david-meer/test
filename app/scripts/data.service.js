(function() {
    'use strict';

    angular
        .module('testApp')
        .service('dataService', dataService);

    dataService.$inject = ['$q'];

    function dataService($q) {
        /*jshint validthis:true */

        this.getRowIds = getRowIds;
        this.getRowDetails = getRowDetails;

        /**
         * Cache the rows retrieved.
         * @type {Array}
         */
        var cache = [];

        /**
         * Get a list of row IDs.
         * @return {Promise} Resolve to an array of row IDs.
         */
        function getRowIds() {
            var deferred = $q.defer();

            // Select random 6 IDs from [1..10] interval.
            deferred.resolve({
                rows: _.sample(_.range(1, 11), 6)
            });

            return deferred.promise;
        }

        /**
         * Retrieve an individual row details.
         * @param  {int} rowId The ID of row to retrieve.
         * @return {Promise}
         */
        function getRowDetails(rowId) {
            var deferred = $q.defer();

            if (_.isUndefined(cache[rowId])) {
                // Generate a random string name.
                var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
                    length = chars.length;
                var name = '';
                for (var i = 0; i < 10; i++) {
                    name += chars.charAt(_.random(0, length - 1));
                }

                // Compose a row.
                var row = {
                    id: rowId,
                    name: name
                };

                // Get the input type values.
                var types = [
                    'text',
                    'select',
                    'radio'
                ];

                row.type = types[_.random(0, 2)];
                if (row.type === 'select') {
                    row.options = [
                        {
                            value: 0,
                            name: 'AngularJS'
                        },
                        {
                            value: 1,
                            name: 'ReactJS'
                        },
                        {
                            value: 2,
                            name: 'BackboneJS'
                        }
                    ];
                } else if (row.type === 'radio') {
                    row.options = [
                        {
                            value: 3,
                            name: 'LESS'
                        },
                        {
                            value: 4,
                            name: 'SCSS'
                        }
                    ];
                }

                // Save to the cache.
                cache[rowId] = row;
            }

            deferred.resolve({
                row: cache[rowId]
            });

            return deferred.promise;
        }
    }
})();
